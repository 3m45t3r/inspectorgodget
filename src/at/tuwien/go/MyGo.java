package at.tuwien.go;

import at.tuwien.go.ai.InspectorGodget;
import games.strategy.engine.gamePlayer.IGamePlayer;
import games.strategy.grid.go.Go;
import games.strategy.grid.go.player.RandomAI;
import games.strategy.grid.player.GridGamePlayer;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Custom launcher to try out our agent
 */
public class MyGo extends Go {
    private static final String HUMAN_PLAYER_TYPE = "Human";
    private static final String AI_TYPE = "InspectorGodget";
    private static final String AI_RANDOM = "AiRandom";


    /**
     * This method should return an array of strings describing the names
     * of different game players, i.e. "human player", "ai1", "ai2", ...
     * For now just return an array with one entry called "human player"
     * or something like that. You can add different AIs to the list later
     *
     * @return available player types
     */
    @Override
    public String[] getServerPlayerTypes() {
        return new String[]{HUMAN_PLAYER_TYPE, AI_TYPE, AI_RANDOM};
    }

    /**
     * This method receives a mapping of player names to player types as a
     * parameter and should create and return IGamePlayer instances based
     * on that mapping. The key strings represent the different player
     * names for the game (for example "Black", "White", ...), the value
     * strings represent the respective game players ("human", "ai1", ..).
     * Iterate over all entries and check if the player type equals the
     * string you added in 2.2.1, if it does create a new instance of
     * "games.strategy.grid.player.GridGamePlayer" with name and type as
     * parameters and add it to the set that you return (you can use a
     * HashSet for that).
     *
     * @param playerNames
     * @return
     */
    @Override
    public Set<IGamePlayer> createPlayers(Map<String, String> playerNames) {
        Set<IGamePlayer> players = new HashSet<IGamePlayer>();
        for (String pName : playerNames.keySet()) {
            String pType = playerNames.get(pName);
            switch (pType) {
                case HUMAN_PLAYER_TYPE:
                    players.add(new GridGamePlayer(pName, pType));
                    break;
                case AI_TYPE:
                    players.add(new InspectorGodget(pName, pType));
                    break;
                case AI_RANDOM:
                    players.add(new RandomAI(pName, pType));
                    break;
                default:
                    break;
            }
        }
        return players;
    }
}
