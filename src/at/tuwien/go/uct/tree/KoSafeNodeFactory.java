package at.tuwien.go.uct.tree;

import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;
import games.strategy.engine.data.Unit;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by stefan on 01.04.15.
 * posted online https://tuwel.tuwien.ac.at/mod/forum/discuss.php?d=56417
 */
public class KoSafeNodeFactory {
    private KoSafeNodeFactory() {
    }

    /**
     * create a node with a ko-safe move, if a ko-situation arises a KoException is thrown.
     * @param gameData gameDate created by move
     * @param pId      id player
     * @param move     initial move
     * @param parent   parent node of the created ko-safe node
     * @return
     * @throws KoException if a ko-situation occurs
     */
    public static Node create(GameData gameData, PlayerID pId, Territory move, Node parent) throws KoException {
        if (isAKoSituation(parent, gameData)) {
            throw new KoException(move);
        }
        return new Node(pId, gameData, parent, move);
    }

    private static boolean isAKoSituation(Node parent, GameData newNodeGameState) {
        if (parent == null || parent.getParent() == null) {
            return false;
        } else {
            List<Territory> newGameState = newNodeGameState.getMap().getTerritories();
            for (int i = 0; i < getState(parent.getParent()).size(); i++) {
                Collection<Unit> a = getState(parent.getParent()).get(i).getUnits().getUnits();
                Collection<Unit> b = newGameState.get(i).getUnits().getUnits();

                Iterator<Unit> uAIt = a.iterator();
                Iterator<Unit> uBIt = b.iterator();
                if (!uAIt.hasNext() && !uBIt.hasNext()) {
                    continue;
                } else if ((!uAIt.hasNext() && uBIt.hasNext()) || (uAIt.hasNext() && !uBIt.hasNext())) {
                    return false;
                } else if (!uAIt.next().getOwner().equals(uBIt.next().getOwner())) {
                    return false;
                }
            }

        }
        return true;
    }


    private static List<Territory> getState(Node node) {
        return node.getGameData().getMap().getTerritories();
    }


    /**
     * used to indicate a ko-situation
     */
    public static class KoException extends Exception {

        private final Territory move;

        public KoException(Territory move) {
            this.move = move;
        }

        public Territory getMove() {
            return move;
        }
    }


}
