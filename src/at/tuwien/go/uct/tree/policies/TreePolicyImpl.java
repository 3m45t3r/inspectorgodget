package at.tuwien.go.uct.tree.policies;

import at.tuwien.go.uct.tree.Node;

import java.util.List;

/**
 * Created by stefan on 03.12.14.
 * implementation of UCB1 TreePolicy
 */
public class TreePolicyImpl implements TreePolicy {
    private static final double BEST_CHILD_CONSTANT = 1 / Math.sqrt(2);

    @Override
    public Node select(Node node) {
        Node n = node;
        while (n.isNotTerminal()) {
            if (n.notFullyExpanded()) {
                return n.expand();
            } else {
                n = descendBestChild(n);
            }
        }
        return n;
    }

    @Override
    public Node descendBestChild(Node node) {
        return descendByUCB1(node);
    }

    private Node descendByUCB1(Node node) {
        List<Node> children = node.getChildren();
        int maxIndex = 0;
        double maxReward = Double.MIN_VALUE;
        for (int i = 0; i < children.size(); i++) {
            Node cN = children.get(i);
            if (!cN.wasVisitedBefore()) {
                return children.get(i); //not visited nodes have the highest value
            } else {
                if (maxReward < cN.tweakedUcb1(BEST_CHILD_CONSTANT)) {
                    maxReward = cN.tweakedUcb1(BEST_CHILD_CONSTANT);
                    maxIndex = i;
                }
            }
        }
        if (children.isEmpty()) {
            return node.expand();
        }
        return children.get(maxIndex);
    }
}
