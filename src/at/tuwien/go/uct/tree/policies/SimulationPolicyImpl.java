package at.tuwien.go.uct.tree.policies;

import at.tuwien.go.game.GameSimulation;
import at.tuwien.go.game.PlayUtil;
import at.tuwien.go.helper.Timer;
import at.tuwien.go.uct.tree.KoSafeNodeFactory;
import at.tuwien.go.uct.tree.Node;
import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;

import java.util.Set;


/**
 * Created by stefan on 04.03.15.
 * is an implementation of a random move policy
 */
public class SimulationPolicyImpl implements SimulationPolicy {

    private static final int TEN_SECONDS = 10000;
    private final PickMovePolicy pickMovePolicy = new RandomPickMovePolicy();

    @Override
    public double simulate(Node node) {
        Node itNode = node;
        Node root = node;
        while (root.getParent() != null) {
            root = root.getParent();
        }
        Timer t = new Timer();
        PlayerID nextPlayer = GameSimulation.getEnemy(itNode.getPlayerID(), itNode.getGameData());
        Set<Territory> validMoves = PlayUtil.getValidMoves(nextPlayer, itNode.getGameData());
        Set<Territory> captureMoves = PlayUtil.getCaptureMoves(nextPlayer, itNode.getGameData());
        boolean update = false;
        while (itNode.isNotTerminal() && t.elapsed() < TEN_SECONDS) {
            try {
                if (update) {
                    nextPlayer = GameSimulation.getEnemy(itNode.getPlayerID(), itNode.getGameData());
                    validMoves = PlayUtil.getValidMoves(nextPlayer, itNode.getGameData());
                    captureMoves = PlayUtil.getCaptureMoves(nextPlayer, itNode.getGameData());
                }
                if (validMoves.isEmpty() && captureMoves.isEmpty()) {
                    break;
                }
                itNode = performPlay(itNode, nextPlayer, validMoves, captureMoves);
                update = true;
            } catch (KoSafeNodeFactory.KoException e) {
                removeMove(e.getMove(), validMoves, captureMoves);
                update = false;
            }
        }
        PlayerID originalPlayer = root.getPlayerID();
        return GameSimulation.getReward(originalPlayer, itNode.getGameData());
    }

    private Node performPlay(Node itNode, PlayerID nextPlayer, Set<Territory> validMoves, Set<Territory> captureMoves) throws KoSafeNodeFactory.KoException {
        Territory move = pickMovePolicy.pickMove(nextPlayer, itNode.getGameData(), validMoves, captureMoves);
        GameData newState = GameSimulation.placeStone(move, nextPlayer, itNode.getGameData());
        if (move != null) {
            return KoSafeNodeFactory.create(newState, nextPlayer, move, itNode);
        }
        return null;
    }

    private void removeMove(Territory move, Set<Territory> validMoves, Set<Territory> captureMoves) {
        validMoves.remove(move);
        captureMoves.remove(move);
    }


}
