package at.tuwien.go.uct.tree.policies;

import at.tuwien.go.game.GameSimulation;
import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;

import java.util.*;

/**
 * Created by stefan on 02.04.15.
 * picks a move depending on its neighborhood
 */
public class NeighborhoodPickMovePolicy implements PickMovePolicy {

    @Override
    public Territory pickMove(final PlayerID forPlayer, final GameData gameData, final Set<Territory> validMoves, final Set<Territory> captureMoves) {
        Territory preferredMove = lookForPreferableMove(forPlayer, gameData, validMoves, captureMoves);
        if (preferredMove != null) {
            return preferredMove;
        }
        if (captureMoves.iterator().hasNext()) {
            return captureMoves.iterator().next();
        } else {
            return validMoves.iterator().next();
        }
    }

    private Territory lookForPreferableMove(final PlayerID playerID, final GameData gameData, final Set<Territory> validMoves, final Set<Territory> captureMoves) {
        List<Territory> sorted = new ArrayList<Territory>() {{
            addAll(validMoves);
            addAll(captureMoves);
        }};
        Collections.sort(sorted, new Comparator<Territory>() {
            @Override
            public int compare(Territory o1, Territory o2) {
                Neighborhood hood1 = new Neighborhood(playerID, gameData, o1);
                Neighborhood hood2 = new Neighborhood(playerID, gameData, o2);
                return Integer.compare(hood2.score(), hood1.score());
            }
        });
        if (new Neighborhood(playerID, gameData, sorted.get(0)).score() > 5) {
            return sorted.get(0);
        }
        return null;
    }

    /**
     * snippet of neighborhood of given territory.
     * The score() method evaluates the neighborhood snippet
     * . . *
     * . * * *
     * * * x * *
     * . * * *
     * . . *
     */
    class Neighborhood {
        private final GameData gameData;
        private final Territory center;
        private final PlayerID playerID;
        private final PlayerID enemy;
        private final Set<Territory> neighbors = new HashSet<>();


        public Neighborhood(PlayerID playerID, GameData gameData, Territory t) {
            this.gameData = gameData;
            this.center = t;
            this.playerID = playerID;
            this.enemy = GameSimulation.getEnemy(playerID, gameData);
            Set<Territory> nearBy = gameData.getMap().getNeighbors(t);
            neighbors.addAll(nearBy);
            for (Territory near : nearBy) {
                neighbors.addAll(gameData.getMap().getNeighbors(near));
            }
            Set<Territory> withOutNearBy = new HashSet<>(neighbors);
            withOutNearBy.remove(nearBy);
            for (Territory territory : withOutNearBy) {
                Set<Territory> farNeighbors = gameData.getMap().getNeighbors(territory);
                neighbors.addAll(farNeighbors);
                for (Territory neighbor : farNeighbors) {
                    neighbors.addAll(gameData.getMap().getNeighbors(neighbor));
                }
            }
        }

        /**
         * evaluates neighborhood
         *
         * @return
         */
        public int score() {
            int score = 0;
            if (is34Move(this.center)) {
                score += 10;
            } else if (is34Enclosure(this.center)) {
                score += 7;
            }
            if (isInterruptingEnemy()) {
                score += 8;
            }
            if (isConnecting()) {
                score += 4;
            }
            for (Territory n : neighbors) {
                if (center.getX() == n.getX() && occupiedBy(n, playerID)) {
                    score += 1;
                }
                if (center.getY() == n.getY() && occupiedBy(n, playerID)) {
                    score += 1;
                }
            }
            score += diagonalConnectionBonus() * 2;
            if (wouldCreateQuadrat()) {
                score -= 10;
            }
            return score;
        }

        private int diagonalConnectionBonus() {
            List<Territory> ref = new ArrayList<>(neighbors);
            for (Territory n : neighbors) {
                if (occupiedBy(n, playerID)) {
                    if (
                            (n.getX() == center.getX() - 1 && n.getY() == center.getY() + 1)
                                    || (n.getX() == center.getX() + 1 && n.getY() == center.getY() - 1)
                                    || (n.getX() == center.getX() - 1 && n.getY() == center.getY() - 1)
                                    || (n.getX() == center.getX() + 1 && n.getY() == center.getY() + 1)) {
                        ref.remove(n);
                    }
                }
            }
            return neighbors.size() - ref.size();
        }

        private boolean occupiedBy(Territory n, PlayerID playerID) {
            if (n.getUnits().getUnits() == null || n.getUnits().getUnits().isEmpty()) {
                return false;
            } else {
                return new ArrayList<>(n.getUnits().getUnits()).get(0).getOwner().equals(playerID);
            }
        }

        private boolean isConnecting() {
            Set<Territory> ref = new HashSet<>(neighbors);
            for (Territory neighbor : neighbors) {
                if (occupiedBy(neighbor, playerID)) {
                    if (neighbor.getX() - 1 == center.getX() && neighbor.getY() == center.getY()) {
                        ref.remove(neighbor);
                    } else if (neighbor.getX() + 1 == center.getX() && neighbor.getY() == center.getY()) {
                        ref.remove(neighbor);
                    } else if (neighbor.getX() == center.getX() && neighbor.getY() - 1 == center.getY()) {
                        ref.remove(neighbor);
                    } else if (neighbor.getX() == center.getX() && neighbor.getY() + 1 == center.getY()) {
                        ref.remove(neighbor);
                    }
                }
            }
            return neighbors.size() - ref.size() == 2;
        }

        private boolean isInterruptingEnemy() {
            Set<Territory> ref = new HashSet<>(neighbors);
            for (Territory neighbor : neighbors) {
                if (occupiedBy(neighbor, enemy)) {
                    if (neighbor.getX() - 1 == center.getX() && neighbor.getY() == center.getY()) {
                        ref.remove(neighbor);
                    } else if (neighbor.getX() + 1 == center.getX() && neighbor.getY() == center.getY()) {
                        ref.remove(neighbor);
                    } else if (neighbor.getX() == center.getX() && neighbor.getY() - 1 == center.getY()) {
                        ref.remove(neighbor);
                    } else if (neighbor.getX() == center.getX() && neighbor.getY() + 1 == center.getY()) {
                        ref.remove(neighbor);
                    }
                }
            }
            return neighbors.size() - ref.size() == 2;
        }

        private boolean wouldCreateQuadrat() {
            boolean x0y1 = false;
            boolean x1y1 = false;
            boolean x1y0 = false;
            boolean xN1y1 = false;
            boolean xN1y0 = false;
            boolean xN1yN1 = false;
            boolean x0yN1 = false;
            boolean x1yN1 = false;
            for (Territory neighbor : neighbors) {
                if (occupiedBy(neighbor, playerID)) {
                    if (neighborRelativeToCenter(neighbor, 0, 1)) {
                        x0y1 = true;
                    } else if (neighborRelativeToCenter(neighbor, 1, 1)) {
                        x1y1 = true;
                    } else if (neighborRelativeToCenter(neighbor, 1, 0)) {
                        x1y0 = true;
                    } else if (neighborRelativeToCenter(neighbor, -1, 1)) {
                        xN1y1 = true;
                    } else if (neighborRelativeToCenter(neighbor, -1, 0)) {
                        xN1y0 = true;
                    } else if (neighborRelativeToCenter(neighbor, -1, -1)) {
                        xN1yN1 = true;
                    } else if (neighborRelativeToCenter(neighbor, 0, -1)) {
                        x0yN1 = true;
                    } else if (neighborRelativeToCenter(neighbor, 1, -1)) {
                        x1yN1 = true;
                    }
                }
            }
            return (x0y1 && x1y1 && x1y0 && !xN1y1 && !xN1y0 && !xN1yN1 && !x0yN1 && !x1yN1) ||
                    (x0y1 && !x1y1 && !x1y0 && xN1y1 && xN1y0 && !xN1yN1 && !x0yN1 && !x1yN1) ||
                    (!x0y1 && !x1y1 && !x1y0 && !xN1y1 && xN1y0 && xN1yN1 && x0yN1 && !x1yN1) ||
                    (!x0y1 && !x1y1 && x1y0 && !xN1y1 && !xN1y0 && !xN1yN1 && x0yN1 && x1yN1);
        }

        private boolean neighborRelativeToCenter(Territory neighbor, int x, int y) {
            return neighbor.getX() == center.getX() + x && neighbor.getY() == center.getY() + y;
        }


        private boolean is34Enclosure(Territory center) {
            Territory move34 = is34InTheNeighborhood(this.neighbors);
            if (move34 != null) {
                return (move34.getX() - 2 == center.getX() && move34.getY() - 2 == center.getY())
                        || (move34.getX() + 2 == center.getX() && move34.getY() - 2 == center.getY())
                        || (move34.getX() - 2 == center.getX() && move34.getY() + 2 == center.getY())
                        || (move34.getX() + 2 == center.getX() && move34.getY() + 2 == center.getY());

            }
            return false;

        }

        private Territory is34InTheNeighborhood(Set<Territory> neighbors) {
            for (Territory neighbor : neighbors) {
                if (is34Move(neighbor)) {
                    return neighbor;
                }
            }
            return null;
        }

        private boolean is34Move(Territory center) {
            int maxX = gameData.getMap().getXDimension();
            int maxY = gameData.getMap().getYDimension();
            return (center.getX() == 2 || center.getX() == maxX - 3)
                    && (center.getY() == 3 || center.getY() == maxY - 4);
        }
    }
}
