package at.tuwien.go.uct.tree.policies;

import at.tuwien.go.uct.tree.Node;

/**
 * Created by stefan on 02.12.14.
 * simulation policy for playouts
 */
public interface SimulationPolicy {

    /**
     * descends the tree till a terminal state is reached then take the score and return it
     * @param node
     * @return
     */
    double simulate(Node node);
}
