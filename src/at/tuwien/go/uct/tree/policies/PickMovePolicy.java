package at.tuwien.go.uct.tree.policies;

import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;

import java.util.Set;

/**
 * Created by stefan on 02.04.15.
 * policy for selecting a move
 */
public interface PickMovePolicy {

    Territory pickMove(PlayerID forPlayer, GameData gameData, Set<Territory> validMoves, Set<Territory> captureMoves);
}
