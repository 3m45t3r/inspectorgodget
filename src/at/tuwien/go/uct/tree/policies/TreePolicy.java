package at.tuwien.go.uct.tree.policies;

import at.tuwien.go.uct.tree.Node;

/**
 * Created by stefan on 02.12.14.
 * node selection policy
 */
public interface TreePolicy {

    /**
     * selects the next child of node n
     * @param n
     * @return a child node
     */
    Node select(Node n);

    /**
     * selects best child of node n
     * @param n
     * @return best child node
     */
    Node descendBestChild(Node n);

}
