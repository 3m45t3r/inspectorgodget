package at.tuwien.go.uct.tree.policies;

import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by stefan on 02.04.15.
 * picks a random move
 */
public class RandomPickMovePolicy implements PickMovePolicy {
    private final Random r = new Random();

    @Override
    public Territory pickMove(PlayerID playerID, GameData gameData, Set<Territory> validMoves, Set<Territory> captureMoves) {
        Territory move = null;

        if (!captureMoves.isEmpty()) {
            move = pickRandom(new ArrayList<>(captureMoves));
        } else if (!validMoves.isEmpty()) {
            move = pickRandom(new ArrayList<>(validMoves));
        }
        return move;
    }

    private Territory pickRandom(List<Territory> moves) {
        return moves.get(r.nextInt(moves.size()));
    }
}
