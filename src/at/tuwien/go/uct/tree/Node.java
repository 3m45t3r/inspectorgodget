package at.tuwien.go.uct.tree;

import at.tuwien.go.game.GameSimulation;
import at.tuwien.go.game.PlayUtil;
import at.tuwien.go.uct.tree.policies.NeighborhoodPickMovePolicy;
import at.tuwien.go.uct.tree.policies.PickMovePolicy;
import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;
import games.strategy.grid.ui.GridPlayData;
import games.strategy.grid.ui.IGridPlayData;

import java.util.*;

/**
 * Created by stefan on 02.12.14.
 * represents a node in the search tree
 */
public class Node {
    private final int TERMINAL_CONSTANT;
    private final Set<Node> children;
    private final PlayerID pId;
    private final GameData gameData;
    private final Set<Territory> captureMoves;
    private final Set<Territory> validMoves;
    private final Node parent;
    private final Territory move;
    private final PickMovePolicy pickMovePolicy;

    private int visited = 0;
    private double reward = 0.0;

    public Node(PlayerID pId, GameData gameData, Node parent, Territory move) {
        this.pickMovePolicy = new NeighborhoodPickMovePolicy();
        this.move = move;
        this.parent = parent;
        this.gameData = gameData;
        this.pId = pId;
        int boardSize = this.gameData.getMap().getXDimension();
        this.validMoves = PlayUtil.getValidMoves(pId, gameData);
        this.captureMoves = PlayUtil.getCaptureMoves(pId, gameData);
        if (boardSize >= 9) {
            TERMINAL_CONSTANT = (boardSize * boardSize) / 5;
        } else {
            TERMINAL_CONSTANT = (boardSize * boardSize) / 3;
        }
        this.children = new HashSet<>();
    }

    public IGridPlayData getMove() {
        return new GridPlayData(move, pId);
    }

    public boolean notFullyExpanded() {
        return children.size() < validMoves.size() + captureMoves.size();
    }

    public boolean wasVisitedBefore() {
        return (visited > 0);
    }

    public boolean isNotTerminal() {
        return !(captureMoves.isEmpty() && validMoves.size() < TERMINAL_CONSTANT);
    }

    public Node expand() {
        while (true) {
            Territory move = pickMovePolicy.pickMove(GameSimulation.getEnemy(pId, gameData), this.gameData, this.validMoves, captureMoves);
            try {
                return makeMove(move);
            } catch (KoSafeNodeFactory.KoException e) {
                this.validMoves.remove(move);
                this.captureMoves.remove(move);
            }
        }
    }

    private Node makeMove(Territory move) throws KoSafeNodeFactory.KoException {
        PlayerID enemy = GameSimulation.getEnemy(pId, gameData);
        GameData newState = GameSimulation.placeStone(move, enemy, gameData);
        Node n = KoSafeNodeFactory.create(newState, enemy, move, this);
        children.add(n);
        return n;
    }


    public List<Node> getChildren() {
        return new ArrayList<>(children);
    }

    public double tweakedUcb1(final double constant) {
        return this.reward / this.visited + Math.sqrt((2 * Math.log(visited)) / visited) * constant;
    }

    public double ucb1() {
        return this.reward / this.visited + Math.sqrt((2 * Math.log(visited)) / visited);
    }

    public double getReward() {
        return reward;
    }

    public int getVisited() {
        return visited;
    }

    public void incrementVisited() {
        visited++;
    }

    public void addReward(double delta) {
        reward += delta;
    }

    public Node getParent() {
        return parent;
    }

    public PlayerID getPlayerID() {
        return pId;
    }

    public GameData getGameData() {
        return gameData;
    }
}
