package at.tuwien.go.uct;

import at.tuwien.go.game.GameSimulation;
import at.tuwien.go.helper.Timer;
import at.tuwien.go.uct.tree.Node;
import at.tuwien.go.uct.tree.policies.SimulationPolicy;
import at.tuwien.go.uct.tree.policies.TreePolicy;
import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.grid.ui.IGridPlayData;

/**
 * implementation of UCT Search Algorithm
 * Created by stefan on 02.12.14.
 */
public class UCTSearchAlgorithm {

    private final TreePolicy treePolicy;
    private final SimulationPolicy simulationPolicy;
    private final int terminationTime;

    public UCTSearchAlgorithm(TreePolicy treePolicy, SimulationPolicy simulationPolicy, int terminationTime) {
        this.treePolicy = treePolicy;
        this.simulationPolicy = simulationPolicy;
        this.terminationTime = terminationTime;
    }

    public IGridPlayData search(PlayerID pId, GameData gameData) {
        Node root = new Node(GameSimulation.getEnemy(pId, gameData), gameData, null, null);
        Timer t = new Timer();
        while (t.elapsed() < terminationTime) {
            Node selectNode = treePolicy.select(root);
            double delta = simulationPolicy.simulate(selectNode);
            backup(selectNode, delta);
        }
        Node bestChild = treePolicy.descendBestChild(root);
        return bestChild.getMove();
    }

    private void backup(Node selectNode, double delta) {
        Node itNode = selectNode;
        double itDelta = delta;
        while (itNode != null) {
            itNode.incrementVisited();
            itNode.addReward(delta);
            itDelta = itDelta * (-1);
            itNode = itNode.getParent();
        }
    }
}
