package at.tuwien.go.ai;

import at.tuwien.go.game.PlayUtil;
import at.tuwien.go.uct.UCTSearchAlgorithm;
import at.tuwien.go.uct.tree.policies.SimulationPolicyImpl;
import at.tuwien.go.uct.tree.policies.TreePolicyImpl;
import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;
import games.strategy.grid.go.delegate.remote.IGoEndTurnDelegate;
import games.strategy.grid.go.delegate.remote.IGoPlayDelegate;
import games.strategy.grid.player.GridAbstractAI;
import games.strategy.grid.ui.GridEndTurnData;
import games.strategy.grid.ui.GridPlayData;
import games.strategy.grid.ui.IGridEndTurnData;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by stefan on 17.11.14.
 * implementation of go player
 */
public class InspectorGodget extends GridAbstractAI {

    public InspectorGodget(String name, String type) {
        super(name, type);
    }

    private static final int FIFTY_SECONDS = 50000;

    @Override
    protected void play() {
        final IGoPlayDelegate playDel = (IGoPlayDelegate) this.getPlayerBridge().getRemoteDelegate();
        final PlayerID me = getPlayerID();
        final GameData data = getGameData();
        Set<Territory> validNonCaptureMoves = PlayUtil.getValidMoves(me, data);
        Set<Territory> validCaptureMoves = PlayUtil.getCaptureMoves(me, data);
        int terminalCount = 2 + data.getMap().getXDimension() * data.getMap().getYDimension() / 5;
        if (validCaptureMoves.size() + validNonCaptureMoves.size() > terminalCount) {
            //terminate under 1 minute see https://tuwel.tuwien.ac.at/mod/forum/discuss.php?d=55306
            playDel.play(new UCTSearchAlgorithm(new TreePolicyImpl(), new SimulationPolicyImpl(), FIFTY_SECONDS).search(me, data));
        } else {
            playDel.play(new GridPlayData(true, me));
        }
    }

    @Override
    protected void endTurn() {
        IGoEndTurnDelegate endTurnDel = (IGoEndTurnDelegate) this.getPlayerBridge().getRemoteDelegate();
        this.pause();
        IGridEndTurnData lastPlayersEndTurn = endTurnDel.getTerritoryAdjustment();
        if (lastPlayersEndTurn == null) {
            GridEndTurnData endPlay = new GridEndTurnData(new HashSet<Territory>(), false, this.getPlayerID());
            endTurnDel.territoryAdjustment(endPlay);
        } else {
            endTurnDel.territoryAdjustment(lastPlayersEndTurn);
        }

    }
}
