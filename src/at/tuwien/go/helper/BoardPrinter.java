package at.tuwien.go.helper;

import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;

/**
 * Created by stefan on 03.03.15.
 * for debugging
 */
public class BoardPrinter {

    private final PlayerID player;

    public BoardPrinter(PlayerID player) {
        this.player = player;
    }

    public void print(String text, GameData data) {
        int i = 0;
        System.out.println("------------" + text + "----------------");
        for (Territory t : data.getMap().getTerritories()) {
            System.out.print(" " + translate(t) + " ");
            i++;
            if (i == data.getMap().getXDimension()) {
                System.out.println();
                i = 0;
            }
        }
    }

    public void printWithMoveToMake(String text, Territory move, GameData data) {
        int i = 0;
        System.out.println("------------" + text + "----------------");
        for (Territory t : data.getMap().getTerritories()) {
            if (move.equals(t)) {
                System.out.print(" # ");
            } else {
                System.out.print(" " + translate(t) + " ");
            }
            i++;
            if (i == data.getMap().getXDimension()) {
                System.out.println();
                i = 0;
            }
        }
    }

    private String translate(Territory t) {
        if (t.getUnits().isEmpty()) {
            return ".";
        }
        PlayerID p = t.getUnits().getUnits().iterator().next().getOwner();
        if (player.equals(p))
            return "x";
        else
            return "o";
    }
}
