package at.tuwien.go.helper;

/**
 * Created by stefan on 02.12.14.
 *
 */
public class Timer {
    private long start;

    public Timer() {
        start = System.currentTimeMillis();
    }

    public long elapsed() {
        return System.currentTimeMillis() - start;
    }

    public void reset() {
        start = System.currentTimeMillis();
    }

    public String toString() {
        // now resets the timer...
        String ret = elapsed() / 1000 + " sec elapsed";
        reset();
        return ret;
    }
}
