package at.tuwien.go.game;

import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.engine.data.Territory;
import games.strategy.grid.go.delegate.PlayDelegate;
import games.strategy.util.Triple;
import games.strategy.util.Tuple;

import java.util.*;

/**
 * Created by stefan on 05.03.15.
 * helper class
 */
public class PlayUtil {

    /**
     * returns a set of moves that would lead to a capture
     * @param playerID
     * @param gameData
     * @return
     */
    public static Set<Territory> getCaptureMoves(PlayerID playerID, GameData gameData) {
        Triple<List<Territory>, List<Tuple<Territory, Collection<Territory>>>, List<Territory>> totalMoves = PlayDelegate.getAllValidMovesCaptureMovesAndInvalidMoves(playerID, gameData);
        List<Tuple<Territory, Collection<Territory>>> captureMove = totalMoves.getSecond();
        Collections.sort(captureMove, getBestCaptureComparator());
        Set<Territory> result = new HashSet<>();
        for (Tuple<Territory, Collection<Territory>> tuple : captureMove) {
            result.add(tuple.getFirst());
        }
        result.removeAll(totalMoves.getThird());
        return result;
    }

    /**
     * returns a set of all valid moves
     * @param playerID
     * @param gameData
     * @return
     */
    public static Set<Territory> getValidMoves(PlayerID playerID, GameData gameData) {
        Triple<List<Territory>, List<Tuple<Territory, Collection<Territory>>>, List<Territory>> totalMoves = PlayDelegate.getAllValidMovesCaptureMovesAndInvalidMoves(playerID, gameData);
        return new HashSet<>(totalMoves.getFirst());
    }

    private static Comparator<Tuple<Territory, Collection<Territory>>> getBestCaptureComparator() {
        return new Comparator<Tuple<Territory, Collection<Territory>>>() {
            public int compare(final Tuple<Territory, Collection<Territory>> t1, final Tuple<Territory, Collection<Territory>> t2) {
                if ((t1 == null && t2 == null) || t1 == t2)
                    return 0;
                if (t1 == null && t2 != null)
                    return 1;
                if (t1 != null && t2 == null)
                    return -1;
                if (t1.equals(t2))
                    return 0;
                final int points1 = t1.getSecond().size();
                final int points2 = t2.getSecond().size();
                if (points1 == points2)
                    return 0;
                if (points1 > points2)
                    return -1;
                return 1;
            }
        };
    }
}
