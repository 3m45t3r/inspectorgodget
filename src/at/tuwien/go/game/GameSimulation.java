package at.tuwien.go.game;

import games.strategy.engine.data.*;
import games.strategy.engine.framework.GameDataUtils;
import games.strategy.grid.go.delegate.EndTurnDelegate;
import games.strategy.grid.go.delegate.PlayDelegate;
import games.strategy.util.Tuple;

import java.util.*;

/**
 * Created by stefan on 02.03.15.
 * posted online https://tuwel.tuwien.ac.at/mod/forum/discuss.php?d=56417
 */
public class GameSimulation {

    public static GameData placeStone(final Territory move, final PlayerID player, final GameData data) {
        if (move == null || player == null || data == null)
            throw new IllegalArgumentException("placeStone can not accept null arguments");
        final Territory newMove;
        final PlayerID newTempPlayer;
        final GameData newData;
        data.acquireReadLock();
        try {
            newData = GameDataUtils.cloneGameData(data, false);
            newData.getDelegateList().addDelegate(getPlayDelegate(data));
            newData.getDelegateList().addDelegate(getEndDelegate(data));
            newMove = (Territory) GameDataUtils.translateIntoOtherGameData(move, newData);
            newTempPlayer = (PlayerID) GameDataUtils.translateIntoOtherGameData(player, newData);
        } finally {
            data.releaseReadLock();
        }
        if (newData == null || newMove == null || newTempPlayer == null)
            throw new IllegalStateException("Game Data translation did not work");
        // assume we have already checked the move is valid, and that any units in the end territory are captured/removed
        final ChangePerformer changePerformer = new ChangePerformer(newData);
        Unit u = newData.getGameLoader().getUnitFactory().createUnit(new StoneType(newData), newTempPlayer, newData);
        newMove.getUnits().getUnits().add(u);
        final Collection<Unit> unitsToMove = new ArrayList<>();
        unitsToMove.add(u);
        for (final Territory t : checkForCaptures(newMove, newTempPlayer, newData)) {
            Territory toRemove = (Territory) GameDataUtils.translateIntoOtherGameData(t, newData);
            changePerformer.perform(ChangeFactory.removeUnits(toRemove, toRemove.getUnits().getUnits()));
        }
        changePerformer.perform(ChangeFactory.addUnits(newMove, unitsToMove));
        return newData;
    }

    private static Collection<Territory> checkForCaptures(final Territory move, final PlayerID player, final GameData data) {
        final PlayerID enemy = getEnemy(player, data);
        Set<Territory> enemyNeighbors = data.getMap().getNeighbors(move, PlayDelegate.TerritoryHasUnitsOwnedBy(enemy));
        List<Set<Territory>> enemyBlocks = new ArrayList<>();
        for (Territory n : enemyNeighbors) {
            enemyBlocks.add(PlayDelegate.getOwnedStoneChainsConnectedToThisTerritory(n, new HashSet<Territory>(), enemy, data, true));
        }
        Set<Territory> captured = new HashSet<>();
        for (Set<Territory> block : enemyBlocks) {
            if (isSurrounded(block, move, data, player)) {
                captured.addAll(block);
            }
        }
        return captured;
    }

    public static double getReward(PlayerID pid, GameData data) {
        Tuple<Tuple<PlayerID, Integer>, Tuple<PlayerID, Integer>> result = GameSimulation.getEndDelegate(data).getFinalScores(new HashSet<Territory>(), data);
        int playerScore;
        int enemyScore;
        if (result.getFirst().equals(pid)) {
            playerScore = result.getFirst().getSecond();
            enemyScore = result.getSecond().getSecond();
        } else {
            playerScore = result.getSecond().getSecond();
            enemyScore = result.getFirst().getSecond();
        }
        if (playerScore > enemyScore) {
            return 1;
        } else if (playerScore < enemyScore) {
            return 0;
        }
        return 0.5;
    }

    private static PlayDelegate getPlayDelegate(GameData data) {
        return (PlayDelegate) data.getDelegateList().getDelegate("play");
    }

    private static EndTurnDelegate getEndDelegate(GameData data) {
        return (EndTurnDelegate) data.getDelegateList().getDelegate("endTurn");
    }

    private static boolean isSurrounded(Set<Territory> block, Territory moveToMake, GameData data, PlayerID enemy) {
        Collection<Territory> neighbors = PlayDelegate.getAllNeighborsOfTerritoryChain(block, data);
        for (Territory n : neighbors) {
            //moveToMake is not occupied so far, but it will if we place the stone, so its considered as occupied by enemy
            if (!n.equals(moveToMake)) {
                Collection<Unit> units = n.getUnits().getUnits();
                if (units.isEmpty()) {
                    return false;
                }
                if (!units.iterator().next().getOwner().equals(enemy)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static PlayerID getEnemy(PlayerID player, GameData gameData) {
        for (PlayerID p : gameData.getPlayerList().getPlayers()) {
            if (!player.equals(p)) {
                return p;
            }
        }
        return null;
    }
}
