package at.tuwien.go.game;

import games.strategy.engine.data.GameData;
import games.strategy.engine.data.UnitType;

/**
 * Created by stefan on 03.03.15.
 * posted online https://tuwel.tuwien.ac.at/mod/forum/discuss.php?d=56417
 */
public class StoneType extends UnitType {
    public StoneType(GameData data) {
        super("stone", data);
    }
}
