# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Go AI Player Implementation for [TripleA Framework](http://triplea.sourceforge.net/mywiki)
* 1.0.0-SNAPSHOT

### Setup ###
1. Change in the project root directory
2. Edit build.xml and set the property "triplea_home"
3. execute `ant`

### Configuration ###
mygo.xml

### Start Game ###
1. `ant execute`
2. Click "Choose Game.." one left side panel
3. Select "MyGo" and click OK
4. Click "Start Local Game"
5. Choose Players and click "Play"


### Dependencies ###
 [TripleA 1.8.0.5](http://sourceforge.net/projects/triplea/files/TripleA/1_8_0_5_extra_maps/)
### How to run tests ###
'ant test'


### Who do I talk to? ###

* Stefan Edenhofer